[![Build Status](https://drone.io/bitbucket.org/duckpuppy/bastions/status.png)](https://drone.io/bitbucket.org/duckpuppy/bastions/latest)

# README #

Have you ever sat in your office and wondered "How on earth can we digitally play Citadels?" Well ... guess what: We did! So that is why we decided to make such a product.

### Compilation ###

Run `./gradlew`
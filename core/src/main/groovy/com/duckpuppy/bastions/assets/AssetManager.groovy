package com.duckpuppy.bastions.assets

import groovy.transform.CompileStatic
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter

@CompileStatic
public class AssetManager {
	private static BitmapFont titleFont = null

	static BitmapFont getTitleFont() {
		if( titleFont == null ) {
			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Blox2.ttf"))
			FreeTypeFontParameter parameter = new FreeTypeFontParameter()
			parameter.size = 60
			titleFont = generator.generateFont(parameter)
			generator.dispose()
			titleFont.setColor(Color.BLACK)
		}

		return titleFont
	}
}

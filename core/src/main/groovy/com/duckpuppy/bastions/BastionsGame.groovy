package com.duckpuppy.bastions

import groovy.transform.CompileStatic
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.duckpuppy.bastions.assets.AssetManager

@CompileStatic
class BastionsGame extends ApplicationAdapter {
	private SpriteBatch batch
	private Texture img
	private BitmapFont font

	@Override
	void create () {
		batch = new SpriteBatch()
		img = new Texture("badlogic.jpg")

		font = AssetManager.titleFont
	}

	@Override
	void render () {
		Gdx.gl.glClearColor(0.75f, 0.75f, 0.75f, 1)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
		batch.begin()
		batch.draw(img, 0, 0)
		font.draw(batch, "BastIons", 300, 400)
		batch.end()
	}
}

package com.duckpuppy.bastions.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.duckpuppy.bastions.BastionsGame

LwjglApplicationConfiguration config = new LwjglApplicationConfiguration()
//DisplayMode desktop = LwjglApplicationConfiguration.getDesktopDisplayMode()
// config.setFromDisplayMode(desktop)
new LwjglApplication(new BastionsGame(), config)
